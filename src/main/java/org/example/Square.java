package org.example;

import java.util.Objects;

public class Square {
    int width;
    int height;

    int calculateAnArea(){
        return this.width * this.height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Square square = (Square) o;
        return width == square.width && height == square.height;
    }

    @Override
    public int hashCode() {
        return Objects.hash(width, height);
    }
}
