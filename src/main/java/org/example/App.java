package org.example;


public class App 
{
    public static void main( String[] args )
    {
        Square myFirstObjectOfASquare = new Square();

        MenuOption intelliJMenuOptions = new MenuOption(
                "File",
                new MenuOption[]
                {
                    new MenuOption("New",
                        new MenuOption[]
                        {
                                new MenuOption("Project")
                        }),
                    new MenuOption("Open"),
                }
        );

        System.out.println( "Hello World!\n" + intelliJMenuOptions.toString() );
    }
}
