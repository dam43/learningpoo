package org.example;

import java.util.Arrays;

public class MenuOption {
    private String name;

    private MenuOption[] options;

    public MenuOption(String name) {
        this(name,null);
    }
    public MenuOption(String name, MenuOption[] options) {
        this.name = name;
        this.options = options;
    }

    @Override
    public String toString() {
        return "MenuOption{\n\t" +
                "name='" + name + '\'' +
                ", \n\toptions=" + Arrays.toString(options) + "\n"+
                '}';
    }
}
